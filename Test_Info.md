## Issues: ##
 - The editor seems to be locked up with the latest code changes. Please investigate and fix.
 - Prior to the introduction of the lock up bug, the game was very slow when running. Please investigate and fix.
 - The existing “Add Robot” button doesn’t seem to function as designed. It should add a basic robot instance to the scene. Please fix and document the cause.
 - The existing “Remove Robot” doesn’t actually remove any robots. This should remove the last robot made. Please fix and document the cause. 

## Feature Requests: ##
 - Implement UI and necessary logic to add a robot to the scene based on selected criteria. Have the added robot reflect that selected creation criterion parameters.
 - Implement UI and systems to delete and create robots over a user-specified duration of time set up in the UI.

## Optimization/Refactoring ##
 - Handle any asset cleanup after instantiation
 - Refactor and improve code to be more readable/maintainable using any manner of techniques/strategies

## Optional ##
- Add any additional functionality/optimization you like
- Fix lighting issues