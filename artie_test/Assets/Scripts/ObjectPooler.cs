using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public Transform poolParent;
    public int maxPoolSize = 10;

    private List<GameObject> pool;

    private void Awake()
    {
        pool = new List<GameObject>();
    }

    public GameObject SpawnInPool(GameObject prefToSpawn)
    {
        return Spawn(prefToSpawn, Vector3.zero, Quaternion.identity, poolParent);
    }

    public GameObject Spawn(GameObject prefToSpawn, Transform parent)
    {
        return Spawn(prefToSpawn, Vector3.zero, Quaternion.identity, parent);
    }

    public GameObject Spawn(GameObject prefToSpawn, Vector3 position, Quaternion rotation, Transform parent)
    {
        GameObject obj;
        if (pool.Count > 0)
        {
            obj = pool[0];
            pool.RemoveAt(0);
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.transform.parent = parent;
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(prefToSpawn, position, rotation, parent);
        }
        return obj;
    }

    public void Despawn(GameObject obj)
    {
        if (obj == null)
            return;

        if (pool.Count < maxPoolSize)
        {
            obj.SetActive(false);
            obj.transform.parent = poolParent;
            pool.Add(obj);
        }
        else
        {
            Destroy(obj);
        }
    }
}
