using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Buffers.Binary;
using System.Threading;
using UnityEditor;
using UnityEngine;

public class Heartbeat : MonoBehaviour
{
    private void Awake()
    {
        //StartCoroutine(ThisIsMyHeartbeat());
    }

    IEnumerator ThisIsMyHeartbeat()
    {
        //while (true)
        //{
            yield return new WaitForEndOfFrame();
            int check = int.MaxValue >> 4;

            var howMany = FindObjectsOfType<GameObject>();

            foreach (var g in howMany)
            {
                List<string> componentTypeNames = new List<string>();
                Debug.Log($"Found object named: {g.name}");
                foreach (var c in g.GetComponents<Component>())
                {
                    componentTypeNames.Add(c.GetType().ToString());
                    Debug.Log($"Adding component {c.GetType().ToString()}");
                }

                Debug.Log("\tIt's components are:");
                foreach (var s in componentTypeNames)
                {
                    Debug.Log($"\t- {s}");
                }

                var guid = GUID.Generate();
                foreach (char c in guid.ToString())
                {
                    Debug.Log(c);
                }

                while (check > 0)
                {
                    check -= 1;
                }
            }
        //}

        yield return null;
    }
}
