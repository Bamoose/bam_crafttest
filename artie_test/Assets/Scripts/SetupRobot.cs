using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupRobot : MonoBehaviour
{
    public RobotSpecs specs;

    [SerializeField] private Transform target;
    private MeshRenderer[] _renderers;
    private List<Material> _allMaterials;

    public LayerMask enemyLayer; 
    
    private void Awake()
    {
        _renderers = GetComponentsInChildren<MeshRenderer>();

        InitMaterials();
    }

    void InitMaterials()
    {
        _allMaterials = new List<Material>();
        //TODO: Eventually we can expand on the project to involve multiple materials for different parts of the robot.
        //For now just look up the first material per renderer for this task.
        for (int i = 0; i < _renderers.Length; i++)
        {
            _allMaterials.Add(_renderers[i].material);
        }
    }

    public void InitRobot()
    {
        if (specs.Equals(default(RobotSpecs)))
        {
            return;
        }
        name += " " + specs.age.ToString();

        for (int i = 0; i < _allMaterials.Count; i++)
        {
            _allMaterials[i].color = specs.color;
        }

        if (specs.scaleFactor != transform.localScale.x)
            transform.ScaleTo(specs.scaleFactor);
    }

    private void OnMouseDown()
    {
        //DrawRay();
        GameManager.Instance.DestroyBy(specs);
    }

    public void DebugSpecs(RobotSpecs specs)
    {
        Debug.Log("-specs-");
        Debug.Log("   age: " + specs.age);
        Debug.Log("   col: " + specs.color);
        Debug.Log("   sca: " + specs.scaleFactor);
        Debug.Log("   pos: " + specs.spawnerIndex);
    }

    /*TODO: finish blowing up bots

    private LineRenderer lineRenderer; 
    private Camera camera;

    private void Start()
    {
        camera = Camera.main;
        lineRenderer = GameManager.Instance.GetLineRenderer();
    }
    private void DrawRay()
    {
        // Cast a ray from the camera to the clicked point in the scene
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Check if the ray hits an object on the "Enemy" layer
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, enemyLayer))
        {

            // Draw a line from the camera to the enemy game object
            lineRenderer.SetPosition(0, camera.transform.position);
            lineRenderer.SetPosition(1, target.position);
        }
    }

    */
}
