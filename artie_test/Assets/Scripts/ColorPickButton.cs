using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorPickButton : MonoBehaviour
{

    public Texture2D colorChart;
    public RectTransform colorChartRect;
    public GameObject chart;

    public RectTransform cursor;
    public Image imgSampleColor;
    public Image cursorColor;

    public void PickColor(BaseEventData data)
    {
        //TODO: update for OnDrag to update color
        PointerEventData pointer = data as PointerEventData;

        cursor.position = pointer.position;

        int xPos = (int)(cursor.localPosition.x * (colorChart.width / colorChartRect.rect.width));
        int yPos = (int)(cursor.localPosition.y * (colorChart.height / colorChartRect.rect.height));

        Color pickedColor = colorChart.GetPixel(xPos, yPos);

        imgSampleColor.color = pickedColor;
        cursorColor.color = pickedColor;
        GameManager.Instance.SetColor(pickedColor);
    }
}
