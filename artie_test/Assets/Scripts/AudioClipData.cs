using UnityEngine;

[CreateAssetMenu(fileName = "AudioClipData", menuName = "ScriptableObjects/AudioClipData", order = 1)]
public class AudioClipData : ScriptableObject
{
    public AudioClip clickUp;
    public AudioClip clickDown;
    public AudioClip buildBot;
    public AudioClip finishedBot;
    public AudioClip destroyBot;

    private Transform _camTrans;
    private Transform camTrans
    {
        get
        {
            if(_camTrans == null)
            {
                _camTrans = Camera.main.transform;
            }
            return _camTrans;
        }
    }

    public void PlayClickUp()
    {
        if (clickUp != null)
        {
            AudioSource.PlayClipAtPoint(clickUp, camTrans.position);
        }
    }

    public void PlayClickDown()
    {
        if (clickDown != null)
        {
            AudioSource.PlayClipAtPoint(clickDown, camTrans.position);
        }
    }

    public void PlayBuildBot()
    {
        if (buildBot != null)
        {
            AudioSource.PlayClipAtPoint(buildBot, camTrans.position);
        }
    }

    public void PlayFinishedBot()
    {
        if (finishedBot != null)
        {
            AudioSource.PlayClipAtPoint(finishedBot, camTrans.position);
        }
    }

    public void PlayDestroyBot()
    {
        if (destroyBot != null)
        {
            AudioSource.PlayClipAtPoint(destroyBot, camTrans.position);
        }
    }
}
