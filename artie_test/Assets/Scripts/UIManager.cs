using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject pnlColorPicker;

    #region singleton

    private static UIManager _instance = null;
    public static UIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    #endregion
        
    [SerializeField] private GameObject uiMainPanel;
    [SerializeField] private GameObject uiCustomizePanel;
    [SerializeField] private GameObject uiDestroyOptionsPanel;

    [SerializeField] private Button uiAddToQueueBtn;
    [SerializeField] private Button uiBuildBtn;
    [SerializeField] private Button uiRemoveBotBtn;
    [SerializeField] private Button uiRemoveFromQueueBtn;
    [SerializeField] private Button uiToggleDestroyOptionsBtn;

    [SerializeField] private TextMeshProUGUI uiScaleFactorTxt;
    [SerializeField] private TextMeshProUGUI uiBotToSpawnTxt;
    [SerializeField] private TextMeshProUGUI uiTimeToSpawnTxt;
    [SerializeField] private TextMeshProUGUI uiSpawnPosTxt;
    [SerializeField] private TextMeshProUGUI uiBuildIndexTxt;
    [SerializeField] private TextMeshProUGUI uiDestroyModeTxt;

    private void Start()
    {
        InitUI();
    }

    private void InitUI()
    {
        ToggleInteractible(eUIButtons.AddToQueue, true);
        ToggleInteractible(eUIButtons.Build, false);
        ToggleInteractible(eUIButtons.RemoveBot, false);
        ToggleInteractible(eUIButtons.RemoveFromQueue, false);

        uiMainPanel.SetActive(true);
        uiCustomizePanel.SetActive(false);
        uiDestroyOptionsPanel.SetActive(false);
    }

    #region Button Events
    public void OnUpdateSpawnPos(int amount)
    {
        PlaySound(amount);
        GameManager.Instance.UpdateSpawnPos(amount);
    }

    public void OnSetBotsToSpawn(int amount)
    {
        PlaySound(amount);
        GameManager.Instance.SetBotsToSpawn(amount);
    }

    public void OnSetTimeToSpawn(int amount)
    {
        PlaySound(amount);
        GameManager.Instance.SetTimeToSpawn(amount);
    }

    public void OnSetScaleFactor(float amount)
    {
        PlaySound(amount);
        GameManager.Instance.SetScaleFactor(amount);        
    }

    public void OnToggleColorPicker()
    {
        AudioManager.Instance.clips.PlayClickUp();
        pnlColorPicker.SetActive(!pnlColorPicker.activeSelf);
        
    }

    public void OnToggleDestroyType()
    {
        AudioManager.Instance.clips.PlayClickUp();
        uiDestroyOptionsPanel.SetActive(!uiDestroyOptionsPanel.activeSelf);
    }    

    public void OnTogglePanels()
    {
        AudioManager.Instance.clips.PlayClickUp();
        uiMainPanel.SetActive(!uiMainPanel.activeSelf);
        uiCustomizePanel.SetActive(!uiCustomizePanel.activeSelf);
        if (uiCustomizePanel.activeSelf)
            GameManager.Instance.VerifyInteractibles();
    }

    public void OnAddBotToQueue()
    {
        AudioManager.Instance.clips.PlayClickUp();
        GameManager.Instance.AddBotToQueue();
    }

    public void OnAddRobot()
    {
        AudioManager.Instance.clips.PlayClickUp();
        GameManager.Instance.AddRobot();
    }

    public void OnRemoveLastRobot()
    {
        AudioManager.Instance.clips.PlayClickUp();
        GameManager.Instance.RemoveLastRobot();
    }

    public void OnDestroyBy(int criteria = 3)
    {
        AudioManager.Instance.clips.PlayClickUp();
        GameManager.Instance.DestroyBy(criteria);
    }

    public void OnRemoveFromQueue()
    {
        AudioManager.Instance.clips.PlayClickDown();
        GameManager.Instance.RemoveFromQueue();
    }
    public void OnSetDestroyMode(int amount)
    {
        PlaySound(amount);
        GameManager.Instance.SetDestroyMode(amount);
    }

    public void OnBuildBots()
    {
        AudioManager.Instance.clips.PlayClickUp();
        GameManager.Instance.BuildBots();
    }

    #endregion

    #region AudioHelpers
    private void PlaySound(float amount)
    {
        if (amount > 0)
        {
            AudioManager.Instance.clips.PlayClickUp();
            return;
        }
        AudioManager.Instance.clips.PlayClickDown();
    }

    private void PlaySound(int amount)
    {
        if (amount > 0)
        {
            AudioManager.Instance.clips.PlayClickUp();
            return;
        }
        AudioManager.Instance.clips.PlayClickDown();
    }
    #endregion

    #region HUD Info

    public void UpdateInfo(eUpdateInfoType type, RobotSpecs specs)
    {
        //This is any value that should be displayed in the UI Hud
        switch (type)
        {
            case eUpdateInfoType.Position:
                uiSpawnPosTxt.text = (specs.spawnerIndex+1).ToString(); //account for 0 base
                break;
            case eUpdateInfoType.Size:
                uiScaleFactorTxt.text = (specs.scaleFactor * 100).ToString("0") + "%";
                break;
        }
        
        
    }

    public void UpdateInfo(eUpdateInfoType type, string amount)
    {
        switch (type)
        {
            case eUpdateInfoType.Bots:
                uiBotToSpawnTxt.text = amount;
                break;
            case eUpdateInfoType.Time:
                uiTimeToSpawnTxt.text = amount;
                break;
            case eUpdateInfoType.DestroyMode:
                uiDestroyModeTxt.text = amount;
                break;
        }
    }

    public void UpdateBuildQueueInfo(string buildInfo)
    {
        uiBuildIndexTxt.text = buildInfo;
    }

    public void ToggleInteractible(eUIButtons btn, bool isUnlocked)
    {
        switch (btn)
        {
            case eUIButtons.AddToQueue:
                uiAddToQueueBtn.interactable = isUnlocked;
                break;
            case eUIButtons.Build:
                uiBuildBtn.interactable = isUnlocked;
                break;
            case eUIButtons.RemoveBot:
                uiRemoveBotBtn.interactable = isUnlocked;
                uiToggleDestroyOptionsBtn.interactable = isUnlocked;
                break;
            case eUIButtons.RemoveFromQueue:
                uiRemoveFromQueueBtn.interactable = isUnlocked;
                break;
        }        
    }

    #endregion

}

public enum eUpdateInfoType
{
    Position,
    Size,
    Time,
    Bots,
    DestroyMode
}

public enum eUIButtons
{
    AddToQueue,
    Build,
    RemoveBot,
    RemoveFromQueue,
}
