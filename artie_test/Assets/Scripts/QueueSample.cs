using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueueSample : MonoBehaviour
{
    public RobotSpecs linkedSpecs;
    [SerializeField] private Button btn;

    public void SetSpecs(RobotSpecs specs)
    {
        linkedSpecs = specs;
        btn.image.color = specs.color;        
    }

    public void OnClick()
    {
        switch (GameManager.Instance.destroyMode)
        {
            case SortCriteria.Age:
                GameManager.Instance.DestroyRobotBy(SortCriteria.Age, linkedSpecs.age.ToString());
                return;
            case SortCriteria.Color:
                GameManager.Instance.DestroyRobotBy(SortCriteria.Color, linkedSpecs.color.ToString());
                return;
            case SortCriteria.Position:
                GameManager.Instance.DestroyRobotBy(SortCriteria.Position, linkedSpecs.spawnerIndex.ToString());
                return;
            case SortCriteria.Size:
                GameManager.Instance.DestroyRobotBy(SortCriteria.Size, linkedSpecs.scaleFactor.ToString());
                return;
        }
    }
}
