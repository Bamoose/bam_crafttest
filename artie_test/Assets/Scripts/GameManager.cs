using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{    
    public ObjectPooler pool;
    public GameObject robotPrefab;
    public RobotSpecs curSpecs;
    public int totalQueued = 0;
    public const int MAXIMUM_SPAWNED_BOTS = 50;
    public Transform spawnerTrans;
    public Transform queueContent;
    public GameObject queueObjPref;
    public SortCriteria destroyMode = new SortCriteria();

    public LineRenderer GetLineRenderer()
    {
        return lineRenderer;
    }

    private List<RobotSpecs> botQueue = new List<RobotSpecs>();
    private List<GameObject> robotInstances = new List<GameObject>();
    private List<GameObject> qSampleInstances = new List<GameObject>();

    [SerializeField] private Transform[] spawnPositions;
    [SerializeField] private int botsToSpawn = 1;
    [SerializeField] private int timeToSpawn = 1; //duration to spawn all bots
    [SerializeField] private float interval = 1; //time between spawns, dynamic to bots to spawn
    [SerializeField] private float minSize = 0.5f;
    [SerializeField] private float maxSize = 3.0f;
    [SerializeField] private LineRenderer lineRenderer;

    private bool isBuilding = false;

    #region singleton and initialization

    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }        
    }

    private void Start()
    {
        curSpecs.scaleFactor = 1;
        curSpecs.color = Color.white;
        curSpecs.age = 0;
        curSpecs.spawnerIndex = 1;
    }


    #endregion


    #region Utility
    private Vector3 GetSpawnPos()
    {
        return spawnPositions[curSpecs.spawnerIndex].position;

    }

    private Vector3 GetSpawnPos(RobotSpecs rSpecs)
    {
        return spawnPositions[rSpecs.spawnerIndex].position;

    }    

    public int GetMaxSpawnPos()
    {
        return spawnPositions.Length - 1;
    }

    private bool IsBotQueueFull()
    {
        if (botQueue.Count >= botsToSpawn)
        {
            return true;
        }

        return false;
    }

    private bool IsSpawnCapMet()
    {
        bool isCapped = (robotInstances.Count >= MAXIMUM_SPAWNED_BOTS);
        //TODO: add player feedback if cap is met
        return isCapped;
    }
    public void DebugList(List<GameObject> list)
    {
        Debug.Log("List count: " + list.Count);
        foreach (var g in list)
        {
            Debug.Log("  -" + g.name);
        }
    }

    

    #endregion

    #region UI Functions

    public void UpdateSpawnPos(int amount)
    {
        curSpecs.spawnerIndex += amount;
        curSpecs.spawnerIndex = Mathf.Clamp(curSpecs.spawnerIndex, 0, GetMaxSpawnPos());

        spawnerTrans.position = GetSpawnPos();

        UIManager.Instance.UpdateInfo(eUpdateInfoType.Position, curSpecs);
    }

    public void SetScaleFactor(float amount)
    {
        curSpecs.scaleFactor += amount;
        curSpecs.scaleFactor = Mathf.Clamp(curSpecs.scaleFactor, minSize, maxSize);

        spawnerTrans.ScaleTo(curSpecs.scaleFactor);

        UIManager.Instance.UpdateInfo(eUpdateInfoType.Size, curSpecs);
    }

    public void DestroyBy(int criteria)
    {
        var crit = (SortCriteria)criteria;
        DestroyRobotBy(crit, GetValueFromCrit(crit, curSpecs));
    }

    private string GetValueFromCrit(SortCriteria crit, RobotSpecs specs)
    {
        string val = string.Empty;
        switch (crit)
        {
            case SortCriteria.Age: val = specs.age.ToString(); break;
            case SortCriteria.Color: val = specs.color.ToString(); break;
            case SortCriteria.Position: val = specs.spawnerIndex.ToString(); break;
            case SortCriteria.Size: val = specs.scaleFactor.ToString(); break;
        }

        return val;
    }

    public void DestroyBy(RobotSpecs specs)
    {
        DestroyRobotBy(destroyMode, GetValueFromCrit(destroyMode, specs), false);
    }

    public void SetBotsToSpawn(int amount)
    {
        botsToSpawn += amount;
        botsToSpawn = Mathf.Clamp(botsToSpawn, 0, MAXIMUM_SPAWNED_BOTS);

        if (botsToSpawn < botQueue.Count)
            botQueue.RemoveRange(botsToSpawn, botQueue.Count - botsToSpawn);

        UIManager.Instance.UpdateInfo(eUpdateInfoType.Bots, botsToSpawn.ToString());

        UIManager.Instance.UpdateBuildQueueInfo(GetQueueString());
    }
    
    public void SetDestroyMode(int amount)
    {
        int mode = (int)destroyMode + amount;
        if (mode < 0) 
            mode = 3;
        else if (mode > 3) 
            mode = 0;
        
        destroyMode = (SortCriteria)mode;

        UIManager.Instance.UpdateInfo(eUpdateInfoType.DestroyMode, destroyMode.ToString());
    }

    public void BuildBots()
    {
        if (!IsBotQueueFull())
            return;

        StartCoroutine("BeginBuilding");
    }

    public void VerifyInteractibles()
    {
        if (isBuilding)
        {
            UIManager.Instance.ToggleInteractible(eUIButtons.AddToQueue, false);
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveFromQueue, false);
            UIManager.Instance.ToggleInteractible(eUIButtons.Build, false);
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveBot, false);
            return;
        }

        if (IsBotQueueFull())
        {
            UIManager.Instance.ToggleInteractible(eUIButtons.AddToQueue, false);
            UIManager.Instance.ToggleInteractible(eUIButtons.Build, true);
        }                       
        else
        {
            UIManager.Instance.ToggleInteractible(eUIButtons.AddToQueue, true);
            UIManager.Instance.ToggleInteractible(eUIButtons.Build, false);
        }     

        if(botQueue.Count > 0)
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveFromQueue, true);
        else
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveFromQueue, false);

        if (IsSpawnCapMet())        
            UIManager.Instance.ToggleInteractible(eUIButtons.Build, false);
        

        if (robotInstances.Count > 0)
        {
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveBot, true);
            if (IsSpawnCapMet())
                UIManager.Instance.ToggleInteractible(eUIButtons.Build, false);            
        }                                    
        else
            UIManager.Instance.ToggleInteractible(eUIButtons.RemoveBot, false);
    }
        
    public void AddBotToQueue()
    {
        if (IsBotQueueFull())
            return;

        totalQueued++;
        curSpecs.age = totalQueued;

        var qObj = pool.Spawn(queueObjPref, queueContent); 

        qObj.GetComponent<QueueSample>().SetSpecs(curSpecs);

        qSampleInstances.Add(qObj);
        botQueue.Add(curSpecs);

        UIManager.Instance.UpdateBuildQueueInfo(GetQueueString());

        VerifyInteractibles();
    }

    public void AddRobot()
    {
        curSpecs.age = totalQueued;

        AddRobot(curSpecs);
    }

    public void AddRobot(RobotSpecs rSpecs)
    {
        if (IsSpawnCapMet()) //TODO: Add player education
            return;

        spawnerTrans.position = GetSpawnPos(rSpecs);

        GameObject g = pool.SpawnInPool(robotPrefab);
        g.transform.position = GetSpawnPos(rSpecs);
        SetupRobot sr = g.GetComponent<SetupRobot>();
        sr.specs = rSpecs;

        robotInstances.Add(g);
        sr.InitRobot();
    }

    private string GetQueueString()
    {
        return botQueue.Count.ToString() + " / " + botsToSpawn.ToString();
    }

    public void SetTimeToSpawn(int amount)
    {
        timeToSpawn += amount;
        timeToSpawn = timeToSpawn < 0 ? 0 : timeToSpawn;

        UIManager.Instance.UpdateInfo(eUpdateInfoType.Time, timeToSpawn.ToString());
    }

    public void RemoveFromQueue()
    {
        int index = qSampleInstances.Count - 1;

        pool.Despawn(qSampleInstances[index]); 
        qSampleInstances.RemoveAt(index);
        botQueue.RemoveAt(index);

        UIManager.Instance.UpdateBuildQueueInfo(GetQueueString());

        VerifyInteractibles();
    }

    public void RemoveLastRobot()
    {
        if (robotInstances.Count < 1)
            return;

        int index = robotInstances.Count - 1;

        pool.Despawn(robotInstances[index]);  
        robotInstances.RemoveAt(index); 

        VerifyInteractibles();
    }

    public void DestroyRobotBy(SortCriteria criteria, string value, bool alsoDestroySample = true)
    {
        int destroyedCount = 0;
        destroyedCount += RemoveMatchingItems(robotInstances, r => MatchRobotSpecs(r.GetComponent<SetupRobot>().specs, criteria, value));
        

        if (alsoDestroySample)
        {
            destroyedCount += RemoveMatchingItems(botQueue, r => MatchRobotSpecs(r, criteria, value));
            destroyedCount += RemoveMatchingItems(qSampleInstances, q => MatchRobotSpecs(q.GetComponent<QueueSample>().linkedSpecs, criteria, value));
        }

        if (destroyedCount > 0)
        {
            AudioManager.Instance.clips.PlayDestroyBot();
            UIManager.Instance.UpdateBuildQueueInfo(GetQueueString());
            VerifyInteractibles();
        }
    }

    private bool MatchRobotSpecs(RobotSpecs specs, SortCriteria criteria, string value)
    {
        switch (criteria)
        {
            case SortCriteria.Age:
                return specs.age == int.Parse(value);
            case SortCriteria.Color:
                return specs.color.ToString() == value;
            case SortCriteria.Position:
                return specs.spawnerIndex == int.Parse(value);
            case SortCriteria.Size:
                return specs.scaleFactor == float.Parse(value);
            default:
                return false;
        }
    }

    private int RemoveMatchingItems<T>(List<T> collection, Func<T, bool> matchFunc)
    {
        int removedCount = 0;
        for (int i = collection.Count - 1; i >= 0; i--)
        {
            if (matchFunc(collection[i]))
            {
                pool.Despawn(collection[i] as GameObject); 
                collection.RemoveAt(i);
                removedCount++;
            }
        }
        return removedCount;
    }

    public void SetColor(Color c)
    {
        curSpecs.color = c;
    }

    #endregion

    #region Coroutines

    private IEnumerator BeginBuilding()
    {

        isBuilding = true;
        VerifyInteractibles();
        interval = timeToSpawn/(float)botQueue.Count;
        var wait = new WaitForSeconds(interval);
        for (int i = 0; i < botQueue.Count; i++)
        {
            AudioManager.Instance.clips.PlayBuildBot();
            AddRobot(botQueue[i]);
            yield return wait;
        }
        isBuilding = false;

        VerifyInteractibles();
        AudioManager.Instance.clips.PlayFinishedBot();
        yield return null;
    }
    #endregion

    #region Unused Methods
    public void GenerateRobotGroup(CreateCriteria criteria, int quantity)
    {
    }

    public void RemoveUsingCriteria(SortCriteria criteria, int removeCount = 1)
    {
    }

    public void SortRobots(SortCriteria criteria)
    {
    }
    #endregion
}

public enum CreateCriteria
{
    Color,
    Size,
    Position
}

public enum SortCriteria
{
    Size,
    Color,
    Position,
    Age
}