using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static void ScaleTo(this Transform transform, float size)
    {
        transform.localScale = new Vector3(size, size, size);
    }
}
