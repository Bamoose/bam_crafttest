using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightUpdater : MonoBehaviour
{
    private const int maxCycleCount = 10;

    void Start()
    {
        StartCoroutine(UpdateLight());
    }

    IEnumerator UpdateLight()
    {
        int curCycle = maxCycleCount;
        while (curCycle > 0)
        {
            var lights = FindObjectsOfType<Light>();

            foreach (var foundLight in lights)
            {
                foundLight.color = Random.ColorHSV();
                curCycle--;
            }
        }
        yield return null;
    }
}
