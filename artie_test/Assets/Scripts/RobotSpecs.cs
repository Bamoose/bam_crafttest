using UnityEngine;

public struct RobotSpecs 
{
    public Color color;
    public float scaleFactor;
    public int age;
    public int spawnerIndex;
}
